class Book{
    constructor({id, author, price, image, title, shortDescription, description}) {
        this.id = id;
        this.author = author;
        this.price = price;
        this.image = image;
        this.title = title
    }
    render(){
        return (
            `<div class="book-item" id="${this.id}">
                <div class="book-img-container">
                    <img src="" alt="img">
                </div>
                <div class="book-item__descr">
                    <p class="book-item__name">${this.title}</p>
                    <p class="book-item__author">${this.author}</p>
                    <div class="book-item_footer">
                        <p class="book-item__price">${this.price} USD</p>
                        <button class="btn btn-light btn-outline-dark btn-view">View</button>
                    </div>
                </div>
            </div>`
        )
    }
}

const container = document.querySelector('.book-list-container')

async function renderBookList () {
    const response = await fetch('books.json')
    const result = await response.json()
    function checkImg (param){
        if (param === ''){
            return '../image/imageNotFound.png'
        } else{
            return param
        }

    }
    const list = result.books.map(el =>{
        return (
            `<div class="book-item" id="${el.id}">
                <div class="book-img-container">
                    <img src="${checkImg(el.image)}" alt="image">
                </div>
                <div class="book-item__descr">
                    <p class="book-item__name">${el.title}</p>
                    <p class="book-item__author">${el.author}</p>
                    <div class="book-item_footer">
                        <p class="book-item__price">${el.price} USD</p>
                        <button class="btn btn-light btn-outline-dark btn-view">View</button>
                    </div>
                </div>
            </div>`
        )
    })
    container.innerHTML=(list.join(''))
}
renderBookList()