export default function Slider(props) {
    const [value, setValue] = useState(props.value)
    useEffect(()=>{
        if(value < 0){
            return setValue(0)
        } else if(value>100){
            return setValue(100)
        }
    }, [value])

    function less (e){
        if(value <= 0){
            setValue(0)
            e.target.disabled = true
        } else{
            e.target.disabled = false
            setValue((p)=>p-1)
        }
    }
    function more (e){
        if(value >= 100){
            setValue(100)
        e.target.disabled = true
    } else{
        e.target.disabled = false
        setValue((p)=>p+1)
    }
}
return (
    <div className={props.className}>
        <div className="progress">
            <div
                className="progress-bar"
                role="progressbar"
                style={{ width: `${value}%` }}
                aria-valuenow="50"
                aria-valuemin="0"
                aria-valuemax="100"
            ></div>
        </div>
        <div className="d-flex justify-content-between mt-2">
            <button type="button" className="btn btn-outline-danger" onClick={less}>
                -
            </button>
            <span>{value}</span>
            <button type="button" className="btn btn-outline-success" onClick={more}>
                +
            </button>
        </div>
    </div>
);
}